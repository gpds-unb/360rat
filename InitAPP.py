
#!/bin/python3
from pickle import NONE, TRUE
from Edition.Interfaces.Init_window import Ui_MainWindow
from Interfaces.Anottation_window import Ui_Anottation
from Service.Rendering import Rendering
from PyQt5 import QtWidgets, QtGui
from PyQt5 import QtCore
from PyQt5 import QtGui
import os
import cv2
import os.path as osp



class InitialWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.ui = Ui_MainWindow()        
        self.ui.setupUi(self)

        #customize window
        self.setWindowIcon(QtGui.QIcon(os.path.join(os.getcwd() + "/Images/icon.png")))
        self.setWindowTitle("360RAT")
        img = Rendering(os.path.join(os.getcwd() + "/Images/icon.png"))
        window_size = (self.ui.program_image.width(),self.ui.program_image.height())
        image = img.image_rendering(window_size)
        self.ui.program_image.setScaledContents(True)
        image_qt = QtGui.QImage(image.data, image.shape[1], image.shape[0], image.shape[1]*3,QtGui.QImage.Format_BGR888)
        pixmap = QtGui.QPixmap(image_qt)
        self.ui.program_image.setPixmap(pixmap)
        self.ui.program_image.repaint()
        QtWidgets.QApplication.processEvents()


        self.ui.button_anot_ROI.clicked.connect(self.chosedAnnotationROI)
        self.ui.button_anot_ROI.setEnabled(True)               
        self.ui.button_anot_edit.clicked.connect(self.chosedAnnotationEdit)
        self.ui.button_anot_edit.setEnabled(True)
                

    def chosedAnnotationROI(self):
        os.system("python " + os.path.join(os.getcwd() + "/360RAT.py"))
        
                
    def chosedAnnotationEdit(self):
        os.system("python " + os.path.join(os.getcwd() + "/Edition" + "/360RAT.py"))
       

          
        
def main():
    app = QtWidgets.QApplication([])
    widget = InitialWindow()
    widget.show()
    app.exec_()

if __name__ == "__main__":
    main()
    


