#!/bin/python3
from pickle import NONE
from Interfaces.Anottation_window_edit import Ui_Anottation
from Interfaces.add_label_window import Ui_AddLabel
from PyQt5 import QtWidgets, QtGui
from PyQt5 import QtCore
from PyQt5 import QtGui
import numpy as np
from Entities.ImageAnotation import Image_Anotation
from Entities.ROI import ROI
from Service.SphereFov import NFOV
from Service.Rendering import Rendering
from Service.Rendering_video import Rendering_Video
from Service.SaveCSV import CSV
from Service.Upload_Annotations import Upload_CSV
from Service.Compose_ROI_controller import ComposeROIController
from Service.Single_ROI_controller import SingleROI
from Service.Scroll_areas import ScrollArea
from Service.Save_windows import SaveWindow
from Service.GetInputUser import GetInput as GetInputFPS

from Edition.Menu import MenuEdition
from functools import partial
from sys import platform
import os
import cv2
import os.path as osp

class AnottationWindow(QtWidgets.QMainWindow):

    def __init__ (self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.ui = Ui_Anottation()
        self.ui.setupUi(self)

        self.setWindowIcon(QtGui.QIcon(os.path.join(os.getcwd() , "Images" , "icon.png")))
        self.path_button_prev = os.path.join(os.getcwd() , "Images" , "icon_prev.png")
        self.path_button_next = os.path.join(os.getcwd() , "Images" , "icon_next.png")

        self.setWindowTitle("360Rat")

        self.set_intial_param()
        self.upload_result_window()
        self.config_buttons_and_sliders()   


    def set_intial_param(self):

        self.list_frame = []
        self.flag_pause=True
        self.flag_Upload_video_init = False
        self.fps = 60
        self.flag_click = False
        self.ui_edit = MenuEdition(self.ui)

        self.nfov = NFOV(400,400)
        self.Scroll_area = ScrollArea(self.ui)
        self.Save_proprieties = SaveWindow()
        self.csv = CSV("User")
        self.window_input_FPS = GetInputFPS()
        self.dictionary_label_color, self.dictionary_label_color_RGB = self.Save_proprieties.set_label_dictionary()
        self.ui_roi, self.ui_compose_roi = self.Save_proprieties.get_ui()

    def config_buttons_and_sliders(self):
        
        #push bottun conect
        self.ui.button_upload_video.triggered.connect(self.upload_video)
        
        self.ui.button_play_video.setStyleSheet(":enabled { background-color: rgb(0, 0, 100);"
                             + " } :disabled { background-color: rgb(143, 143, 143);}")
        self.ui.button_pause_video.clicked.connect(self.pause)
        self.ui.button_pause_video.setStyleSheet(":enabled { background-color: rgb(0, 0, 100);"
                             + " } :disabled { background-color: rgb(143, 143, 143);}")

        #buttons single roi
        self.ui.button_save_ROI.clicked.connect(self.open_menu_edit)
        self.ui.button_save_ROI.setStyleSheet(":enabled { background-color: rgb(0, 0, 100);"
                             + " } :disabled { background-color: rgb(143, 143, 143);}")

        self.ui.button_previous.setStyleSheet(":enabled { background-color: rgb(255, 255, 255);"
                             + " } :disabled { background-color: rgb(143, 143, 143);}")
        self.ui.button_previous.setIcon(QtGui.QIcon(self.path_button_prev))
            
        self.ui.button_next.setStyleSheet(":enabled { background-color: rgb(255, 255, 255);"
                             + " } :disabled { background-color: rgb(143, 143, 143);}")
        self.ui.button_next.setIcon(QtGui.QIcon(self.path_button_next))

        
        self.ui.button_previous.setEnabled(False)
        self.ui.button_next.setEnabled(False)
        self.ui.button_play_video.setEnabled(False)
        self.ui.button_pause_video.setEnabled(False)

        ##sliders
        self.ui.slider_fov_h.valueChanged.connect(self.update_fov)
        self.ui.slider_fov_w.valueChanged.connect(self.update_fov)
        self.ui.slider_pos_x.valueChanged.connect(self.update_pos_xy)
        self.ui.slider_pos_y.valueChanged.connect(self.update_pos_xy)
        self.ui.slider_fov_h.setEnabled(False)
        self.ui.slider_fov_w.setEnabled(False)
        self.ui.slider_pos_x.setEnabled(False)
        self.ui.slider_pos_y.setEnabled(False)        

        self.ui.input_ROI_W.editingFinished.connect(self.update_fov_W_input)
        self.ui.input_ROI_H.editingFinished.connect(self.update_fov_H_input)
        self.ui.input_ROI_W.setEnabled(False)
        self.ui.input_ROI_H.setEnabled(False)

    def close_upload_window(self):
        self.window_upload_result.hide()

    def upload_result_window(self):
        self.window_upload_result = QtWidgets.QDialog()
        self.window_upload_result.setWindowIcon(QtGui.QIcon(os.path.join(os.getcwd() , "Images" , "icon.png")))        
        self.window_upload_result.setWindowTitle("360Rat")
        self.ui_upload = Ui_AddLabel()
        self.ui_upload.setupUi(self.window_upload_result)
        self.ui_upload.pushButton_OK.clicked.connect(self.close_upload_window)

    def open_menu_edit(self):
        self.ui_edit.get_ui().show()
        self.setEnabled(False)

    def CloseEvent(self, event):
         self.setEnabled(True)

   
    def previous_image(self):
        try: 
            self.id_image -= 1
            self.ui.button_next.setEnabled(True)
            self.ui.button_play_video.setEnabled(True)

            if self.id_image == 0:
                self.ui.button_previous.setEnabled(False)
            self.ui.slider_video_duration.blockSignals(True)
            self.ui.slider_video_duration.setValue(self.id_image)
            self.ui.slider_video_duration.blockSignals(False)
            self.set_image_equirectangular_view(self.id_image)

        except Exception as e:
            print(e)
            print(self.id_image)

    def next_image(self):
        try: 
            self.id_image += 1
            self.ui.button_previous.setEnabled(True)

            if (len(self.list_frame) - self.id_image) == 1:
                self.ui.button_next.setEnabled(False)
                self.finish_video = True
                self.pause()
            self.ui.slider_video_duration.blockSignals(True)
            self.ui.slider_video_duration.setValue(self.id_image)
            self.ui.slider_video_duration.blockSignals(False)
            self.set_image_equirectangular_view(self.id_image)
        except Exception as e:
            self.finish_video = True
            self.pause()
            self.ui.button_next.setEnabled(False)
            self.id_image -= 1

    def set_image_equirectangular_view(self, id):

        self.ui.equi_image.setScaledContents(True)
        img_equi_qt = QtGui.QImage(self.list_frame[id].get_image().data, self.list_frame[id].get_image().shape[1], self.list_frame[id].get_image().shape[0], self.list_frame[id].get_image().shape[1]*3,QtGui.QImage.Format_BGR888)
        pixmap = QtGui.QPixmap(img_equi_qt)
        self.ui.equi_image.setPixmap(pixmap)
        self.ui.equi_image.repaint()
        QtWidgets.QApplication.processEvents()
        self.ui.equi_image.mousePressEvent = self.get_pos_ROI
        self.ui.text_frame_number.setText(f'Frame: {self.id_image}')
        self.ui.text_time.setText(f'{self.id_image/self.fps :}')

        #set image in nfov
        self.img_copy = self.list_frame[id].get_image().copy()
        self.nfov(self.img_copy)
        self.ui.perspective_image.clear()
        self.ui.slider_fov_h.setEnabled(False)
        self.ui.slider_fov_w.setEnabled(False)
        self.ui.slider_pos_x.setEnabled(False)
        self.ui.slider_pos_y.setEnabled(False)
        self.ui.input_ROI_W.setEnabled(False)
        self.ui.input_ROI_H.setEnabled(False)

        self.Scroll_area.clear_scroll_area_single_ROI()
        self.nfov.nfov_id = 1
        self.flag_click = False
        
        #add in scroll area roi saved previous
        for roi in self.list_frame[id].get_list_roi():
            self.add_ROI_in_scroll_area(roi)
            self.nfov.set_fov(roi.get_fov()[1], roi.get_fov()[0])
            self.nfov.updateNFOV(roi.get_center_point())
            self.nfov.draw_NFOV_edges(self.img_copy, label_color= self.dictionary_label_color[roi.get_label()])
        
        #print image with roi already marked
        if self.list_frame[id].get_list_roi():
            image = self.img_copy
            image_with_roi = QtGui.QImage(image.data, image.shape[1] , image.shape[0], image.shape[1]*3, QtGui.QImage.Format_BGR888)
            pixmap_roi = QtGui.QPixmap(image_with_roi)
            self.ui.equi_image.setPixmap(pixmap_roi)
            self.ui.equi_image.repaint()

            self.nfov.nfov_id = self.list_frame[id].get_list_roi()[-1].get_id() + 1
        
        #Change the display information about roi
        self.ui.text_nfov_information.setText(" Actual ROI: ")

    #Video
    def upload_video(self):

        video_path = QtWidgets.QFileDialog.getOpenFileName(self, 'Open File', 'c\\', 'Image files ( *.mp4)')

        if video_path[0] !='':
            self.window_input_FPS.set_text_window("Enter FPS of video:")
            window = self.window_input_FPS.get_window()
            ui_input = self.window_input_FPS.get_ui()
            ui_input.button_OK.clicked.connect(partial(self.rendering_video, video_path))
            window.show()
            window.closeEvent = self.CloseEvent
            self.setEnabled(False)   

        '''else:
            self.ui_upload.text_result.setText("Upload Canceled!")
            self.window_upload_result.show()'''

    def rendering_video(self, video_path):

        self.window_input_FPS.close_window()
 
        self.list_frame.clear()
        self.Scroll_area.clear_scroll_area_compose_ROI()
        self.id_image = 0
        self.ui.button_play_video.setEnabled(True)  
        self.finish_video = False
        self.ui.button_previous.clicked.connect(self.previous_image)
        self.ui.button_next.clicked.connect(self.next_image)
        self.ui.button_next.setEnabled(True)
        self.ui.button_previous.setEnabled(False)
        self.flag_Upload_video_init = True
            
        redenring_video = Rendering_Video(video_path)
        self.list_frame, self.path_frames_original = redenring_video.get_list_video()
        self.ui.button_play_video.clicked.connect(self.start)
            
        self.set_image_equirectangular_view(self.id_image)  
        self.ui.slider_video_duration.setMaximum((len(self.list_frame)-1))    
        self.ui.slider_video_duration.valueChanged.connect(self.change_frame_video)

    def timerEvent(self):        
        self.time = self.time.addMSecs(self.Msec)
        self.ui.text_time.setText(self.time.toString("mm:ss"))

    def start(self):
        self.update_fps()
        self.timer = QtCore.QTimer()
        self.time = QtCore.QTime(0, 0, 0)
        self.timer.timeout.connect(self.next_image)
        self.timer.setInterval(int(1000 / self.fps))
        self.timer.start()     
        self.ui.button_play_video.setEnabled(False)
        self.ui.button_pause_video.setEnabled(True)
        self.flag_pause=False
        self.Msec = int((1/self.fps) *1000)
    
    def update_fps(self):
        try:
            self.fps = int(self.window_input_FPS.get_input_text())
            
        except ValueError as ex:
            self.fps = 60
            self.ui_upload.text_result.setText("Fps not valid!")
            self.window_upload_result.show()

    def pause(self):
        try:
            self.timer.stop()
            if self.finish_video == False :
                self.ui.button_play_video.setEnabled(True)
            self.ui.button_pause_video.setEnabled(False)
            self.flag_pause=True
        except Exception as e:
            self.ui.button_previous.setEnabled(True)
            print(e)

    def change_frame_video(self):
        self.id_image = self.ui.slider_video_duration.value()
        self.ui.button_next.setEnabled(True)
        self.ui.button_play_video.setEnabled(True)
        if (len(self.list_frame) - self.id_image) == 1:
            self.ui.button_next.setEnabled(False)
            self.ui.button_play_video.setEnabled(False)
        if self.id_image == 0:
            self.ui.button_previous.setEnabled(False)
       
        #self.set_frame_equirectangular_view(self.id_image)
        self.set_image_equirectangular_view(self.id_image)

    #ROI propreties
    def get_pos_ROI(self, event):
        x = event.pos().x()
        y = event.pos().y()

        self.ui.slider_pos_x.blockSignals(True)
        self.ui.slider_pos_y.blockSignals(True)
        self.ui.slider_pos_x.setValue(x)
        self.ui.slider_pos_y.setValue(y)
        self.ui.slider_pos_x.blockSignals(False)
        self.ui.slider_pos_y.blockSignals(False)

        self.center_point = np.array([x/self.ui.equi_image.width(), y/self.ui.equi_image.height()])
        self.update_view()
        self.flag_click = True
        
        if self.flag_Upload_video_init == True :
            self.flag_Upload_video_init = False

        self.ui.slider_pos_x.setEnabled(True)
        self.ui.slider_pos_y.setEnabled(True)
        self.ui.slider_fov_h.setEnabled(True)
        self.ui.slider_fov_w.setEnabled(True)
        self.ui.input_ROI_W.setEnabled(True)
        self.ui.input_ROI_H.setEnabled(True)
        
        if self.flag_pause == False:
            self.pause()

    def update_view(self, label_color = [0, 0, 255]):
        
        #update NFOV
        perspective_img, equi_img = self.nfov.updateNFOV(self.center_point, label_color)

        #update equi image on interface
        img_equi_qt = QtGui.QImage(equi_img.data, equi_img.shape[1], equi_img.shape[0], equi_img.shape[1]*3,QtGui.QImage.Format_BGR888)
        pixmap_equi = QtGui.QPixmap(img_equi_qt)
        self.ui.equi_image.setPixmap(pixmap_equi)
        self.ui.equi_image.repaint()

        #update perspective image on interface
        perspective_img_qt = QtGui.QImage(perspective_img.data, perspective_img.shape[1], 
                                        perspective_img.shape[0], perspective_img.shape[1]*3,QtGui.QImage.Format_BGR888)
        pixmap_perspective = QtGui.QPixmap(perspective_img_qt)
        self.ui.perspective_image.setPixmap(pixmap_perspective)
        self.ui.perspective_image.repaint()

        self.display_roi_information()

    def display_roi_information(self):
        #write nfov information in interface
        ang_H = 2 * np.arctan((self.nfov.FOV[1]*self.nfov.PI_2)/2) * 100
        ang_W = 2 * np.arctan((self.nfov.FOV[0]*self.nfov.PI)/2) * 100
        
        
        nfov_information = (" Actual ROI: "+
                            #f'\n          Field of View ~= width:{ang_W :.2f} | height{ang_H :.2f}' +
                            f'\n          ROI ~= W:{self.nfov.FOV[0] :.2f} | H{self.nfov.FOV[1] :.2f}' +
                            "\n          Position: X:{:.3f}, Y: {:.3f}".format(self.center_point[0], self.center_point[1]))

        self.ui.text_nfov_information.setText(nfov_information)

    def update_fov_W_input(self):
        
        if self.ui.input_ROI_W.text() == '':
            return

        fov_w = 2 * np.tan(float(self.ui.input_ROI_W.text())/200)

        if fov_w > (self.ui.slider_fov_w.maximum()/100) or fov_w < 0:
            self.ui_upload.text_result.setText("Value not accept!")
            self.window_upload_result.show()            

        else :
            self.ui.slider_fov_w.setValue(int(fov_w * 100))
            fov_h = self.ui.slider_fov_h.value()/100
            self.nfov.set_fov(fov_h, fov_w)

            self.update_view()

    def update_fov_H_input(self):

        if self.ui.input_ROI_H.text() == '':
            return
        
        fov_h = 2 * np.tan(float(self.ui.input_ROI_H.text())/200) 

        if fov_h > (self.ui.slider_fov_h.maximum()/100) or fov_h < 0:
            self.ui_upload.text_result.setText("Value not accept!")
            self.window_upload_result.show()

        else :
            self.ui.slider_fov_h.setValue(int(fov_h * 100))
            fov_w = self.ui.slider_fov_w.value()/100
            self.nfov.set_fov(fov_h, fov_w)

            self.update_view()

    def update_fov(self):
        fov_h = self.ui.slider_fov_h.value()/100
        fov_w = self.ui.slider_fov_w.value()/100
        self.nfov.set_fov(fov_h, fov_w)

        self.update_view()

    def update_pos_xy(self):
        x = self.ui.slider_pos_x.value()/self.ui.equi_image.width()
        y = self.ui.slider_pos_y.value()/self.ui.equi_image.height()
        self.center_point = np.array([x, y])
        
        self.update_view()


def main():
    app = QtWidgets.QApplication([])
    widget = AnottationWindow()
    widget.showMaximized()
    app.exec_()

if __name__ == "__main__":
    main()







