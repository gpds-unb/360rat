#!/bin/python3
from pickle import NONE
from Interfaces.menu_edition import Ui_menuEdit
from PyQt5 import QtWidgets, QtGui
from PyQt5 import QtCore
from PyQt5 import QtGui
from functools import partial
from sys import platform
import os
import cv2
import os.path as osp

class MenuEdition():
    def __init__(self, ui):

        self.ui_main = ui
        #set window of menu
        self.window_edit = QtWidgets.QMainWindow()
        self.window_edit.setWindowIcon(QtGui.QIcon(os.path.join(os.getcwd() , "Images" , "icon.png")))                
        self.window_edit.setWindowTitle("360Rat")
        self.ui_edit = Ui_menuEdit()
        self.ui_edit.setupUi(self.window_edit)

        self.ui_edit.button_save_edition.clicked.connect(self.save_edition)


    def get_ui(self):
        return self.window_edit

    def save_edition(self):
        print("Save")



